import XCTest
import Foundation
import Testing
import HTTP
@testable import Vapor
@testable import App

/// This file shows an example of testing 
/// routes through the Droplet.

class RouteTests: TestCase {
    let drop = try! Droplet.testable()
    
    // Test the GET "leagues" endpoint
    func testLeagues() throws {
        try drop
            .testResponse(to: .get, at: "leagues")
            .assertStatus(is: .ok)
    }
    
    // Test the GET "league/<id>" endpoint
    func testLeagueId() throws {
        try drop
            .testResponse(to: .get, at: "league/id")
            .assertStatus(is: .ok)
    }

    // Test the GET "teams" endpoint
    func testTeams() throws {
        try drop
            .testResponse(to: .get, at: "teams")
            .assertStatus(is: .ok)
    }
    
    // Test the GET "team/<id>" endpoint
    func testTeamId() throws {
        try drop
            .testResponse(to: .get, at: "team/id")
            .assertStatus(is: .ok)
    }
    
    // Test the GET "teams/league/<id>" endpoint
    func testTeamsLeagueId() throws {
        try drop
            .testResponse(to: .get, at: "teams/league/id")
            .assertStatus(is: .ok)
    }
}

// MARK: Manifest

extension RouteTests {
    /// This is a requirement for XCTest on Linux
    /// to function properly.
    /// See ./Tests/LinuxMain.swift for examples
    static let allTests = [
        ("testLeagues", testLeagues),
        ("testLeagueId", testLeagueId),
        ("testTeams", testTeams),
        ("testTeamId", testTeamId),
        ("testTeamsLeagueId", testTeamsLeagueId),
    ]
}
