import Vapor

extension Droplet {
    func setupRoutes() throws {

        // Get all Teams
        get("teams") { req in
            let teams = try self.database?.driver.raw("SELECT * from Team")
            var json = JSON()
            try json.set("teams", teams)
            return json
        }
        
        // Get a specific Team
        get("team", ":id") { req in
            guard let teamId = req.parameters["id"]?.int else {
                return "invalid team id provided"
            }
            let team = try self.database?.driver.raw("SELECT * from Team WHERE id == \(teamId)")
            var json = JSON()
            try json.set("team", team)
            return json
        }

        // Get all Teams belonging to a specific League
        get("teams","league", ":id") { req in
            guard let leagueId = req.parameters["id"]?.int else {
                return "invalid league id provided"
            }
            let teams = try self.database?.driver.raw("SELECT * from Team WHERE league_id == \(leagueId)")
            var json = JSON()
            try json.set("teams", teams)
            return json
        }
        
        // Get all Leagues
        get("leagues") { req in
            let leagues = try self.database?.driver.raw("SELECT * from League")
            var json = JSON()
            try json.set("leagues", leagues)
            return json
        }
        
        // Get a specific League
        get("league", ":id") { req in
            guard let leagueId = req.parameters["id"]?.int else {
                return "invalid league id provided"
            }
            let league = try self.database?.driver.raw("SELECT * from League WHERE id == \(leagueId)")
            var json = JSON()
            try json.set("league", league)
            return json
        }
    }
}
