//
//  League.swift
//  App
//
//  Created by Wandeep Basra on 08/03/2018.
//

import Vapor
import FluentProvider
import HTTP

final class League: Model {
    let storage = Storage()
    
    // MARK: Properties and database keys
    
    /// The content of the team object
    var name: String
    
    /// Constants for the column names in the database
    struct Keys {
        static let id = "id"
        static let name = "name"
    }
    
    /// Creates a new League
    init(name: String) {
        self.name = name
    }
    
    // MARK: Fluent Serialization
    
    /// Initializes the League from the
    /// database row
    init(row: Row) throws {
        name = try row.get(League.Keys.name)
    }
    
    // Serializes the League to the database
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(League.Keys.name, name)
        return row
    }
}

// MARK: Fluent Preparation

extension League: Preparation {
    /// Prepares a table/collection in the database
    /// for storing Leagues
    static func prepare(_ database: Database) throws {
        try database.create(self) { builder in
            builder.id()
            builder.string(League.Keys.name)
        }
    }
    
    /// Undoes what was done in `prepare`
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

// MARK: JSON

// How the model converts from / to JSON.
// For example when:
//     - Creating a new Team (POST /teams)
//     - Fetching a team (GET /teams, GET /team/:id)
//
extension League: JSONConvertible {
    convenience init(json: JSON) throws {
        self.init(
            name: try json.get(League.Keys.name)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set(League.Keys.id, id)
        try json.set(League.Keys.name, name)
        return json
    }
}

// MARK: HTTP

// This allows Post models to be returned
// directly in route closures
extension League: ResponseRepresentable { }

