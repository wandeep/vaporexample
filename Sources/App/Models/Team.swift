//
//  Team.swift
//  App
//
//  Created by Wandeep Basra on 08/03/2018.
//
import Vapor
import FluentProvider
import HTTP

final class Team: Model {
    let storage = Storage()
    
    // MARK: Properties and database keys
    
    /// The content of the team object
    var name: String
    var website: String
    var league_id: Int
    
    
    /// Constants for the column names in the database
    struct Keys {
        static let id = "id"
        static let name = "name"
        static let website = "website"
        static let league_id = "league_id"
    }
    
    /// Creates a new Team
    init(name: String, website: String, league_id: Int) {
        self.name = name
        self.website = website
        self.league_id = league_id
    }
    
    // MARK: Fluent Serialization
    
    /// Initializes the Team from the
    /// database row
    init(row: Row) throws {
        name = try row.get(Team.Keys.name)
        website = try row.get(Team.Keys.website)
        league_id = try row.get(Team.Keys.league_id)
    }
    
    // Serializes the Team to the database
    func makeRow() throws -> Row {
        var row = Row()
        try row.set(Team.Keys.name, name)
        try row.set(Team.Keys.website, website)
        try row.set(Team.Keys.league_id, league_id)
        return row
    }
 
}

// MARK: Fluent Preparation

extension Team: Preparation {
    /// Prepares a table/collection in the database
    /// for storing Teams
    static func prepare(_ database: Database) throws {
        try database.create(self) { builder in
            builder.id()
            builder.string(Team.Keys.name)
            builder.string(Team.Keys.website)
            builder.int(Team.Keys.league_id)
        }
    }
    
    /// Undoes what was done in `prepare`
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

// MARK: JSON

// How the model converts from / to JSON.
// For example when:
//     - Creating a new Team (POST /teams)
//     - Fetching a team (GET /teams, GET /team/:id)
//
extension Team: JSONConvertible {
    convenience init(json: JSON) throws {
        self.init(
            name: try json.get(Team.Keys.name),
            website: try json.get(Team.Keys.website),
            league_id: try json.get(Team.Keys.league_id)
        )
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set(Team.Keys.id, id)
        try json.set(Team.Keys.name, name)
        try json.set(Team.Keys.website, website)
        try json.set(Team.Keys.league_id, league_id)
        return json
    }
}

// MARK: HTTP

// This allows Post models to be returned
// directly in route closures
extension Team: ResponseRepresentable { }


