**VaporExample**

A sample backend using the Vapor web framework for Swift

Provides the following RESTful web endpoints on the base URL: https://wandeep.vapor.cloud

 - GET `/leagues` retrieve all the league items 
 - GET `/teams`  retrieve all the team items 
 - GET `/league/<id>` retrieve a league item for the specified league `<id>` 
 - GET `/team/<id>` retrieve a team item for the specified team `<id>` 
 - GET `/teams/league/<id>` retrieve all the teams for the specified league `<id>`

Includes unit tests for the routing functionality